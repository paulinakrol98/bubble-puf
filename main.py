import pygame as pg
import random
import math
pg.init()
window_xy = (800,600)

pink = (207, 89, 116)
blue = (4, 71, 116)
green = (5, 150, 89)
red = (175, 15, 19)
light_red = (255, 45, 46)
purple = (95, 41, 182)
light_blue = (199, 229, 255)
black = (0,0,0)
grey = (38, 43, 44)




gameDisplay = pg.display.set_mode(window_xy)
pg.display.set_caption('Bubble Puf')
clock = pg.time.Clock()



def Text_draw(x,y,color,text,text_size):
  MyFont = pg.font.SysFont('Comic Sans MS',text_size)
  title_text = MyFont.render(text,True,color)
  gameDisplay.blit(title_text,(x,y))

def text_rect_draw(x,y,text_color,text,text_size,rect_color,rect_h,rect_w,rect_type):
  pg.draw.rect(gameDisplay, rect_color, (x,y,rect_w,rect_h),rect_type)
  Text_draw(x,y,text_color,text,text_size)

def button(x,y,text_color_a,text_color_d,text,text_size,rect_color_a,rect_color_d,rect_h,rect_w,rect_type_a,rect_type_d,mouse,action=None):
  click = pg.mouse.get_pressed()
  if x + rect_w > mouse[0] > x and y + rect_h > mouse [1] > y:
      text_rect_draw(x,y,text_color_a,text,text_size,rect_color_a,rect_h,rect_w,rect_type_a)
      if click[0] == 1 and action != None:
        action()
  else:
    text_rect_draw(x,y,text_color_d,text,text_size,rect_color_d,rect_h,rect_w,rect_type_d)
  
def bubble_rand_color():
  i = random.randint(1,4)
  if i == 1:
    return purple
  elif i == 2:
    return pink
  elif i == 3:
    return blue
  elif i == 4:
    return green
  
def bubble_generator(last_raw,last_id,start_y,r,d):
  bubble_list = []
  for y in range(start_y,0,-d):
    for x in range(0,780,d):
      bubble_x = int(x + r)
      bubble_y = int(y + r)
      bubble_list.append({
        'x' : bubble_x,
        'y' : bubble_y,
        'color' : bubble_rand_color(),
        'id' : last_id,
        'row_id' : last_raw,
        'r' : r
      })          
      last_id += 1
    last_raw += 1
  return bubble_list
     
def draw_bubble(bubbles_list):
  for bubble_dict in bubbles_list:
    
    position = [bubble_dict['x'],bubble_dict['y']]
    color = bubble_dict['color']
    r = bubble_dict['r']
    pg.draw.circle(gameDisplay, color, position, r)

def add_row(bubble_list,last_raw,r,d,last_id):
  raw = last_raw
  for i in bubble_list:
    i['y']+=40
    y = i['y']
  for x in range(0,780,d):
    bubble_x = int(x + r)
    bubble_list.append({
      'x' : bubble_x,
      'y' : 40,
      'color' : bubble_rand_color(),
      'id' : last_id,
      'row_id' : raw,
      'r' : r
    }) 
    last_id += 1
  last_raw += 1
  return bubble_list

def new_row(bubble_list,last_raw,r,d,move_number,last_id):
  if move_number == 6:
    bubble_list = add_row(bubble_list,last_raw,r,d,last_id)
  elif move_number == 10:
    bubble_list = add_row(bubble_list,last_raw,r,d,last_id)
  elif move_number == 13:
    bubble_list = add_row(bubble_list,last_raw,r,d,last_id)
  elif move_number == 15:
    bubble_list = add_row(bubble_list,last_raw,r,d,last_id)
  elif move_number >= 16:
    bubble_list = add_row(bubble_list,last_raw,r,d,last_id)
  return bubble_list

def exit_game():
  pg.quit()
  quit()

def menu():
  work = True
  while work:
    gameDisplay.fill(light_blue)
    mouse = pg.mouse.get_pos()
    
    for event in pg.event.get():
      if event.type == pg.QUIT:
        work = False
      if event.type == pg.KEYDOWN:
        if event.key == pg.K_ESCAPE:
          work = False

    Text_draw(200,50,(0,0,0),'Bubble Puf',80)
    Text_draw(650,575,(0,0,0),'Paulina Król v1.0',20)
    
    button(285,200,grey,black,'New game',40,light_red,red,70,215,0,2,mouse,game)
    button(285,280,grey,black,'High Score',40,light_red,red,70,215,0,2,mouse,high_score)
    button(285,360,grey,black,'Instruction',40,light_red,red,70,215,0,2,mouse,instruction)
    button(285,470,grey,black,'Exit',40,light_red,red,70,215,0,2,mouse,exit_game)

    pg.display.update()
    clock.tick(60)

def game():
  work = True
  game_new = True
  x_speed = 0
  y_speed = 0
  last_id = 0
  last_raw = 0
  move_number = 0
  start_y = 340
  r = 20
  d = 2*r
  shot_bubble = [{
        'x' : 380,
        'y' : 580,
        'color' : bubble_rand_color(),
        'r' : 20
      }]
  while work:
    gameDisplay.fill(light_blue)
    mouse = pg.mouse.get_pos()

    if game_new == True:
      bubbles_list = bubble_generator(last_raw,last_id,start_y,r,d)
      game_new = False

    draw_bubble(bubbles_list)
    draw_bubble(shot_bubble)


    for event in pg.event.get():
      if event.type == pg.QUIT:
        work = False
      if event.type == pg.KEYDOWN:
        if event.key == pg.K_ESCAPE:
          work = False
      if event.type == pg.MOUSEBUTTONDOWN:
        delta_x = mouse[0] - shot_bubble[0]['x']
        delta_y = mouse[1] - shot_bubble[0]['y']

        if delta_x == 0:
          x_speed = 0
          y_speed = 10
        elif delta_y == 0:
          x_speed = 1
          y_speed = 2
        else:
          x_speed = math.floor(math.pi * math.atan2(delta_x,delta_y))
          y_speed = math.floor(math.pi * math.atan2(delta_y,delta_x))
          print(x_speed,y_speed)
        move_number += 1
        print(move_number)
        bubbles_list = new_row(bubbles_list,last_raw,r,d,move_number,last_id)

    
    if x_speed != 0 and y_speed != 0:
      shot_bubble[0]['x'] += x_speed
      shot_bubble[0]['y'] += y_speed
    if shot_bubble[0]['y'] <= 400:
      x_speed = 0
      y_speed = 0
      shot_bubble[0]['y'] = 400

    draw_bubble(shot_bubble)
    pg.display.update()
    clock.tick(60)

def high_score():
  work = True
  while work:
    gameDisplay.fill(light_blue)
    mouse = pg.mouse.get_pos()
    
    for event in pg.event.get():
      if event.type == pg.QUIT:
        work = False
      if event.type == pg.KEYDOWN:
        if event.key == pg.K_ESCAPE:
          work = False
    
    pg.display.update()
    clock.tick(1)
    
def instruction():
  work = True
  while work:
    gameDisplay.fill(light_blue)
    mouse = pg.mouse.get_pos()
    
    for event in pg.event.get():
      if event.type == pg.QUIT:
        work = False
      if event.type == pg.KEYDOWN:
        if event.key == pg.K_ESCAPE:
          work = False
    
    pg.display.update()
    clock.tick(60)


menu()
pg.quit()
quit()